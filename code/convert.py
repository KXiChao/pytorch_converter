import os
import torch
import torch._utils
try:
    torch._utils._rebuild_tensor_v2
except AttributeError:
    def _rebuild_tensor_v2(storage, storage_offset, size, stride, requires_grad, backward_hooks):
        tensor = torch._utils._rebuild_tensor(storage, storage_offset, size, stride)
        tensor.requires_grad = requires_grad
        tensor._backward_hooks = backward_hooks
        return tensor
    torch._utils._rebuild_tensor_v2 = _rebuild_tensor_v2

import torchvision

from ConvertModel import ConvertModel_ncnn

from ReplaceDenormals import ReplaceDenormals

import models.crnn as crnn

pytorch_net= crnn.CRNN(32, 1, 37, 256)
model_path = './crnn.pth'

#if torch.cuda.is_available():
#    pytorch_net = pytorch_net.cuda()
print('loading pretrained model from %s' % model_path)
pytorch_net.load_state_dict(torch.load(model_path))
dst = 'ncnn'
#ReplaceDenormals(pytorch_net)
print('Converting...')

InputShape = (1, 1, 32, 100)
text_net, binary_weights = ConvertModel_ncnn(pytorch_net, InputShape, softmax=False)

NetName = str(pytorch_net.__class__.__name__)
ModelDir = './models'
import numpy as np
with open(ModelDir + NetName + '/' + NetName + '.param', 'w') as f:
    f.write(text_net)
with open(ModelDir + NetName + '/' + NetName + '.bin', 'w') as f:
    for weights in binary_weights:
        for blob in weights:
            blob_32f = blob.flatten().astype(np.float32)
            blob_32f.tofile(f)
print('Converting Done.')


