import caffe
import cv2
from craft import CRAFT
import numpy as np
import imgproc
import torch
from torch.autograd import Variable
from collections import OrderedDict

#pytorch func
def copyStateDict(state_dict):
    if list(state_dict.keys())[0].startswith("module"):
        start_idx = 1
    else:
        start_idx = 0
    new_state_dict = OrderedDict()
    for k, v in state_dict.items():
        name = ".".join(k.split(".")[start_idx:])
        print(name)
        if 'num_batches_tracked' in name:
            continue
        new_state_dict[name] = v
    return new_state_dict

def graph_torch(cuda):
        # resize
    net = CRAFT()     # initialize

    print('Loading weights from checkpoint (' + model_path + ')')
    if cuda:
        net.load_state_dict(copyStateDict(torch.load(model_path)))
    else:
        net.load_state_dict(copyStateDict(torch.load(model_path, map_location=lambda storage, loc: storage)))

    if cuda:
        net = net.cuda()
        net = torch.nn.DataParallel(net)
        #cudnn.benchmark = False

    print("have loaded!")

    image = imgproc.loadImage(image_file)
    img_resized, target_ratio, size_heatmap = imgproc.resize_aspect_ratio(image, 300, interpolation=cv2.INTER_LINEAR, mag_ratio=1.5)
    ratio_h = ratio_w = 1 / target_ratio

    # preprocessing
    x = imgproc.normalizeMeanVariance(img_resized)
    x = torch.from_numpy(x).permute(2, 0, 1)    # [h, w, c] to [c, h, w]
    x = Variable(x.unsqueeze(0))                # [c, h, w] to [b, c, h, w]
    if cuda:
        x = x.cuda()
    
    return net,x

#pytroch forward
model_path = './craft_mlt_25k.pth'
image_file = './demo.jpg'
cuda = False
pytorch_net,x = graph_torch(cuda)
pytorch_net.eval()
pytorch_output,_ = pytorch_net(x)
pytorch_output = pytorch_output.data.cpu().numpy()

#caffe forward 
model_def = './modelsCRAFT/CRAFT.prototxt'
model_weights = './modelsCRAFT/CRAFT.caffemodel'
caffe.set_device(0)
caffe.set_mode_gpu()
caffe_net = caffe.Net(model_def, model_weights, caffe.TEST)
input_name = 'data'
output_name = 'ConvNd_27'
caffe_net.blobs['data'].data[...] = x.cpu().numpy()
caffe_net.forward(start=input_name)
caffe_output = caffe_net.blobs[output_name].data




#print and compare the results
print('  caffe: min: {}, max: {}, mean: {}'.format(caffe_output.min(), caffe_output.max(), caffe_output.mean()))
print('pytorch: min: {}, max: {}, mean: {}'.format(pytorch_output.min(), pytorch_output.max(), pytorch_output.mean()))
diff = np.abs(pytorch_output - caffe_output)
print('   diff: min: {}, max: {}, mean: {}, median: {}'.format(diff.min(), diff.max(), diff.mean(), np.median(diff)))
