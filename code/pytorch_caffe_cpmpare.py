# -*- coding: utf-8
from pytorch2caffe import plot_graph, pytorch2caffe
import sys
#sys.path.append('/media/xichao/Experiment/Programs/PytorchConverter0/code/pytorch2caffe/caffe/distribute/python/')
#sys.path.insert(0, '/media/xichao/Experiment/Programs/PytorchConverter0/code/pytorch2caffe/caffe/distribute/python/')
import os
os.environ['GLOG_minloglevel'] = '2'
import caffe
import numpy as np


import torch
from torch import nn
from torch.autograd import Variable
import torchvision
#from models import Encoder
from resnet import Encoder
device = 'cuda'
import torchvision.transforms as transforms
import skimage.transform
from scipy.misc import imread, imresize
img = imread("/media/xichao/share3/imagecaption/images/image_0.jpg")
if len(img.shape) == 2:
    img = img[:, :, np.newaxis]
    img = np.concatenate([img, img, img], axis=2)
img = imresize(img, (256, 256))
img = img.transpose(2, 0, 1)
img = img / 255.
img = torch.FloatTensor(img).to(device)
normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])
transform = transforms.Compose([normalize])
image = transform(img)
image = image.unsqueeze(0)  # (1, 3, 256, 256)

print(type(image))
#np.savetxt("./image.txt", image.cpu().numpy().reshape(-1).astype(np.float32), fmt="%f")
print("max: {}, min: {}".format(image.cpu().numpy().reshape(-1).max(), image.cpu().numpy().reshape(-1).min()))
# test the model or generate model
test_mod = True

caffemodel_dir = '/media/xichao/share3/imagecaption/caffe_model'
input_size = (1, 3, 256, 256)

model_def = os.path.join(caffemodel_dir, 'Sequential_1.prototxt')
model_weights = os.path.join(caffemodel_dir, 'encoder.caffemodel')
# input_name = 'ConvNdBackward1'
# output_name = 'AddmmBackward348'

# pytorch net
# model = torchvision.models.inception_v3(pretrained=True, transform_input=False)
# model.eval()

# random input
#image = np.random.randint(0, 255, input_size)
#input_data = image.astype(np.float32)

model_path = './BEST_checkpoint_.pth.tar'

model = Encoder();

checkpoint = torch.load(model_path, map_location=lambda storage, loc: storage)
'''
encoder_dict = {}
for name, parma in checkpoint['encoder'].state_dict().items():
    if 'num_batches' not in name and  'ada' not in name:
        encoder_dict[name.replace('resnet', '0')] = parma
#        encoder_dict[name] = parma
#        print(name)

modules = list(model.children())#[:-1]
#    print(modules)
model = nn.Sequential(*modules)

for name,a in modules[0].named_children():
    print(name)

model.load_state_dict(encoder_dict)
# modules = list(checkpoint['encoder'].children())[:-1]


model.eval()
'''
#input_var = Variable(torch.from_numpy(input_data))

if not test_mod:
    # generate caffe model
    output_var = model(input_var)
#    plot_graph(output_var, os.path.join(caffemodel_dir, 'pytorch_graph.dot'))
    pytorch2caffe(input_var, output_var, model_def, model_weights)
    exit(0)

# test caffemodel
caffe.set_device(0)
caffe.set_mode_gpu()
net = caffe.Net(model_def, model_weights, caffe.TEST)
input_name = 'data'
output_name = 'bottleout32relu'
net.blobs['data'].data[...] = image.cpu().numpy()
net.forward(start=input_name)
caffe_output = net.blobs[output_name].data
print('  caffe: min: {}, max: {}, mean: {}'.format(caffe_output.min(), caffe_output.max(), caffe_output.mean()))
#print(type(caffe_output), caffe_output.shape)
#np.savetxt("./encoder_out.txt", caffe_output.flatten().astype(np.float32), fmt="%f")
'''
with open("./encoder_out.txt", 'w') as f:
    data = caffe_output.flatten().astype(np.float32)
    data.tofile(f)
'''
print("caffe_output\n", caffe_output[0,0,0,:])

model = checkpoint["encoder"].cuda().eval()
#input_var = input_var.cuda()
output_var = model(image)#.permute(0, 2, 3, 1) 
pytorch_output = output_var.data.cpu().numpy()
print('pytorch_output\n', pytorch_output.shape, "\n", pytorch_output[0,0,0,0:100])
'''
#print("caffe_output\n", caffe_output[0,0,0,:])
print(image.shape, pytorch_output.shape, caffe_output.shape)
print('pytorch: min: {}, max: {}, mean: {}'.format(pytorch_output.min(), pytorch_output.max(), pytorch_output.mean()))
print('  caffe: min: {}, max: {}, mean: {}'.format(caffe_output.min(), caffe_output.max(), caffe_output.mean()))

#diff = np.abs(pytorch_output - caffe_output)
#print('   diff: min: {}, max: {}, mean: {}, median: {}'.format(diff.min(), diff.max(), diff.mean(), np.median(diff)))
'''
