import os
import torch
import torch._utils
try:
    torch._utils._rebuild_tensor_v2
except AttributeError:
    def _rebuild_tensor_v2(storage, storage_offset, size, stride, requires_grad, backward_hooks):
        tensor = torch._utils._rebuild_tensor(storage, storage_offset, size, stride)
        tensor.requires_grad = requires_grad
        tensor._backward_hooks = backward_hooks
        return tensor
    torch._utils._rebuild_tensor_v2 = _rebuild_tensor_v2

import torchvision
from skimage import io

from ConvertModel import ConvertModel_caffe

from ReplaceDenormals import ReplaceDenormals
import cv2
from craft import CRAFT
import numpy as np
import imgproc
from torch.autograd import Variable
from collections import OrderedDict

model_path = './craft_mlt_25k.pth'
image_file = './demo.jpg'
cuda = False

def copyStateDict(state_dict):
    if list(state_dict.keys())[0].startswith("module"):
        start_idx = 1
    else:
        start_idx = 0
    new_state_dict = OrderedDict()
    for k, v in state_dict.items():
        name = ".".join(k.split(".")[start_idx:])
        print(name)
        if 'num_batches_tracked' in name:
            continue
        new_state_dict[name] = v
    return new_state_dict

def graph_torch():
        # resize
    net = CRAFT()     # initialize

    print('Loading weights from checkpoint (' + model_path + ')')
    if cuda:
        net.load_state_dict(copyStateDict(torch.load(model_path)))
    else:
        net.load_state_dict(copyStateDict(torch.load(model_path, map_location=lambda storage, loc: storage)))

    if cuda:
        net = net.cuda()
        net = torch.nn.DataParallel(net)
        #cudnn.benchmark = False

    print("have loaded!")

    image = imgproc.loadImage(image_file)
    img_resized, target_ratio, size_heatmap = imgproc.resize_aspect_ratio(image, 300, interpolation=cv2.INTER_LINEAR, mag_ratio=1.5)
    ratio_h = ratio_w = 1 / target_ratio

    # preprocessing
    x = imgproc.normalizeMeanVariance(img_resized)
    x = torch.from_numpy(x).permute(2, 0, 1)    # [h, w, c] to [c, h, w]
    x = Variable(x.unsqueeze(0))                # [c, h, w] to [b, c, h, w]
    if cuda:
        x = x.cuda()
    
    return net,x


'''
import models.crnn as crnn
pytorch_net= crnn.CRNN(32, 1, 37, 256)
model_path = './crnn.pth'
print(torch.load(model_path))
'''
#checkpoint = torch.load(model_path, map_location=lambda storage, loc: storage)
#decoder = checkpoint['decoder']
#decoder = decoder.to(device)
#decoder.eval()
#encoder = checkpoint['encoder']
#encoder = encoder.to(device)
#encoder.eval()
'''
if torch.cuda.is_available():
    pytorch_net = pytorch_net.cuda()
print('loading pretrained model from %s' % model_path)
pytorch_net.load_state_dict(torch.load(model_path))
'''

print('Converting...')
net,x = graph_torch()
net.eval()
#InputShape = (1, 3, 256, 256)
text_net, binary_weights = ConvertModel_caffe(net, x, softmax=False)

NetName = str(net.__class__.__name__)
ModelDir = './models'
import numpy as np
import google.protobuf.text_format
with open(ModelDir + NetName + '/' + NetName + '.prototxt', 'w') as f:
    f.write(google.protobuf.text_format.MessageToString(text_net))
with open(ModelDir + NetName + '/' + NetName + '.caffemodel', 'wb') as f:
    f.write(binary_weights.SerializeToString())
print('Converting Done.')

