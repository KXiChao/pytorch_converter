import os

import torch
import torch.backends.cudnn as cudnn

image_h = image_w = image_size = 256
channel = 3
epochs = 10000
patience = 10
num_train_samples = 591753
num_valid_samples = 25014
max_len = 57
# captions_per_image = 5

# Model parameters
emb_dim = 512  # dimension of word embeddings
attention_dim = 512  # dimension of attention linear layers
decoder_dim = 512  # dimension of decoder RNN
dropout = 0.5
#device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')  # sets device for model and PyTorch tensors
cudnn.benchmark = True  # set to true only if inputs to model are fixed size; otherwise lot of computational overhead

# Training parameters
start_epoch = 0
epochs = 120  # number of epochs to train for (if early stopping is not triggered)
epochs_since_improvement = 0  # keeps track of number of epochs since there's been an improvement in validation BLEU
batch_size = 32
workers = 0  # for data-loading; right now, only 1 works with h5py
encoder_lr = 1e-4  # learning rate for encoder if fine-tuning
decoder_lr = 4e-4  # learning rate for decoder
grad_clip = 5.  # clip gradients at an absolute value of
alpha_c = 1.  # regularization parameter for 'doubly stochastic attention', as in the paper
best_bleu4 = 0.  # BLEU-4 score right now
print_freq = 100  # print training/validation stats every __ batches
fine_tune_encoder = False  # fine-tune encoder?
checkpoint = None  # path to checkpoint, None if none
min_word_freq = 3

# Data parameters
coco_path = '/home/lab305/data/coco'  # coco数据集所在的根目录
annotation_path = os.path.join(coco_path, 'annotations')  # 注释的路径
annotation_train_file = os.path.join(annotation_path, 'captions_train2017.json')  # caption_train 的文件
train_images_folder = os.path.join(coco_path, 'train2017')
annotation_val_file = os.path.join(annotation_path, 'captions_val2017.json')
val_images_folder = os.path.join(coco_path, 'val2017')
