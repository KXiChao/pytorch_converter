
import numpy as np
import sys
import caffe 
#root='/home/xxx/'   
deploy='./crnn.prototxt'    
caffe_model='./CRNN.caffemodel'   
img='./demo.png'   
#labels_filename = './labels.txt'  

net = caffe.Net('./crnn.prototxt', 1, weights='./CRNN.caffemodel')   


transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape}) 
transformer.set_transpose('data', (2,0,1))   
#transformer.set_mean('data', np.load(mean_file).mean(1).mean(1))   
transformer.set_raw_scale('data', 255)   
transformer.set_channel_swap('data', (2,1,0))   

im=caffe.io.load_image(img)                  
net.blobs['data'].data[...] = transformer.preprocess('data',im)     


out = net.forward()



print(out)


