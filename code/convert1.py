import os
import torch
import torch._utils
try:
    torch._utils._rebuild_tensor_v2
except AttributeError:
    def _rebuild_tensor_v2(storage, storage_offset, size, stride, requires_grad, backward_hooks):
        tensor = torch._utils._rebuild_tensor(storage, storage_offset, size, stride)
        tensor.requires_grad = requires_grad
        tensor._backward_hooks = backward_hooks
        return tensor
    torch._utils._rebuild_tensor_v2 = _rebuild_tensor_v2

import torchvision

from ConvertModel import ConvertModel_caffe

from ReplaceDenormals import ReplaceDenormals
'''
import models.crnn as crnn
pytorch_net= crnn.CRNN(32, 1, 37, 256)
model_path = './crnn.pth'
print(torch.load(model_path))
'''
'''
model_path = './BEST_checkpoint_.pth.tar'
device = 'cuda'
checkpoint = torch.load(model_path, map_location=lambda storage, loc: storage)
decoder = checkpoint['decoder']
#decoder = decoder.to(device)
decoder.eval()
encoder = checkpoint['encoder']
#encoder = encoder.to(device)
#encoder.eval()
'''
from models import Encoder as pymodel

pytorch_net = pymodel()
model_path = './BEST_checkpoint_.pth.tar'
#if torch.cuda.is_available():
#    pytorch_net = pytorch_net.cuda()
print('loading pretrained model from %s' % model_path)
param_dict = {}
#pytorch_net = torch.load('BEST_checkpoint_.pth.tar', map_location=lambda storage, loc: storage)'')
for name, param in torch.load(model_path, map_location=lambda storage, loc: storage)['encoder'].state_dict().items():
        if 'num_batches_tracked' in name:
                continue
        else:
                param_dict[name] = param
                print(name)

pytorch_net.load_state_dict(param_dict)

print("--------------------------model_graph--------------------------")
for name, params in pytorch_net.state_dict().items():
        print(name)
dst = 'caffe'
#ReplaceDenormals(pytorch_net)
print('Converting...')

InputShape = (1, 3, 256, 256)
text_net, binary_weights = ConvertModel_caffe(pytorch_net, InputShape, softmax=False)

ModelDir = './caffemodels'
NetName = "encoder"
import numpy as np
import google.protobuf.text_format
with open(ModelDir + '/' + NetName + '.prototxt', 'w') as f:
    f.write(google.protobuf.text_format.MessageToString(text_net))
with open(ModelDir + '/' + NetName + '.caffemodel', 'w') as f:
    f.write(binary_weights.SerializeToString())
print('Converting Done.')

