import os
import torch
import torch._utils
try:
    torch._utils._rebuild_tensor_v2
except AttributeError:
    def _rebuild_tensor_v2(storage, storage_offset, size, stride, requires_grad, backward_hooks):
        tensor = torch._utils._rebuild_tensor(storage, storage_offset, size, stride)
        tensor.requires_grad = requires_grad
        tensor._backward_hooks = backward_hooks
        return tensor
    torch._utils._rebuild_tensor_v2 = _rebuild_tensor_v2

import torchvision

from ConvertModel import ConvertModel_ncnn

from ReplaceDenormals import ReplaceDenormals

import models as ic
device = "cpu"
pytorch_net= ic.Encoder();
model_path = './BEST_checkpoint_.pth.tar'

#if torch.cuda.is_available():
#    pytorch_net = pytorch_net.cuda()
print('loading pretrained model from %s' % model_path)

stat_dict = {}
for i,v in torch.load("encoder.pt").items():
    if "num_batches_tracked" not in i:
        print(i)
        stat_dict[i] = v
#    else:
#        print("aaaaaaa" + i)
        
pytorch_net.load_state_dict(stat_dict)
dst = 'ncnn'
#ReplaceDenormals(pytorch_net)
print('Converting...')
#pytorch_net = torch.load('BEST_checkpoint_.pth.tar', map_location=lambda storage, loc: storage)
#encoder = encoder.to(device)
#encoder.eval()
InputShape = (1, 3, 256, 256)

text_net, binary_weights = ConvertModel_ncnn(pytorch_net, InputShape, softmax=False)

NetName = 'imagecaption_encoder'#str(pytorch_net.__class__.__name__)
ModelDir = './modelss'
import numpy as np
with open(ModelDir + NetName + '/' + NetName + '.param', 'w') as f:
    f.write(text_net)
with open(ModelDir + NetName + '/' + NetName + '.bin', 'w') as f:
    for weights in binary_weights:
        for blob in weights:
            blob_32f = blob.flatten().astype(np.float32)
            blob_32f.tofile(f)
print('Converting Done.')


